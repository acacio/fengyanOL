#coding:utf8
'''
Created on 2011-5-26

@author: sean_lan
'''
from app.scense.applyInterface import shop
from app.scense.serverconfig.node import nodeHandle
from app.scense.netInterface.pushObjectNetInterface import pushOtherMessage
from app.scense.protoFile.shop import buyItemInMyshop_pb2
from app.scense.protoFile.shop import GetNpcShopInfo206_pb2
from app.scense.protoFile.shop import NpcShopSellItem220_pb2
from app.scense.protoFile.shop import NpcShopBuyItem_pb2

@nodeHandle
def getMallItemInfo_208(dynamicId,request_proto):
    '''获取商城信息'''
    pushOtherMessage(905, u'该功能暂未开放,敬请期待V1.7版本', [dynamicId])

@nodeHandle
def buyItemInMyshop_213(dynamicId,request_proto):
    '''购买商店中的物品'''
    argument = buyItemInMyshop_pb2.buyItemInMyshopRequest()
    argument.ParseFromString(request_proto)
    response = buyItemInMyshop_pb2.buyItemInMyshopResponse()
    
    dynamicId = dynamicId
    characterId = argument.id
    itemTemplateId = argument.itemTemplateId
    data = shop.buyItemInMyshop(dynamicId, characterId, itemTemplateId)
    response.result = data.get('result',False)
    response.message = data.get('message','')
    return response.SerializeToString()

@nodeHandle
def buyItemInMall_214(dynamicId,request_proto):
    '''购买商城中的物品'''
    pushOtherMessage(905, u'该功能暂未开放,敬请期待V1.7版本', [dynamicId])

@nodeHandle
def getMallItemTime_1605(dynamicId,request_proto):
    '''返回该特价商品的打折剩余时间'''

    pushOtherMessage(905, u'该功能暂未开放,敬请期待V1.7版本', [dynamicId])

@nodeHandle
def getNpcShopInfo_206(dynamicId,request_proto):
    '''获取公共商店信息'''
    argument=GetNpcShopInfo206_pb2.getNpcShopInfoRequest()
    argument.ParseFromString(request_proto)
    response = GetNpcShopInfo206_pb2.getNpcShopInfoResponse()
    
    dynamicId = dynamicId
    characterId = argument.id
    npcId = argument.npcId
    shopCategory = argument.shopCategory
    curPage = argument.curPage
    result = shop.getNpcShopInfo(dynamicId, characterId, npcId, shopCategory,curPage)
    response.result = result.get('result',False)
    response.message = result.get('message','')
    if result.get('data',None):
        data = result.get('data')
        response.data.shopCategory = data.get('shopCategory',0)
        response.data.curPage = data.get('curPage',0)
        response.data.maxPage = data.get('maxPage',1)
        for _item in data.get('items',[]):
            item = response.data.packageItemInfo.add()
            item.remainTime = _item.get('overTime',0)
            _item['item'].SerializationItemInfo(item.itemInfo)
            
    return response.SerializeToString()

@nodeHandle
def NpcShopSellItem_220(dynamicId,request_proto):
    '''出售物品'''
    argument=NpcShopSellItem220_pb2.npcShopSellItemRequest()
    argument.ParseFromString(request_proto)
    response = NpcShopSellItem220_pb2.npcShopSellItemResponse()
    
    dynamicId = dynamicId
    characterId = argument.id
    itemPos = argument.itemPos
    packageType = argument.packageType
    curpage = argument.curpage
    stack = argument.sellCount
    result = shop.NpcShopSellItem(dynamicId, characterId, itemPos, packageType, curpage,stack)
    response.result = result.get('result',False)
    response.message = result.get('message','')
    response.data.packageType = packageType
    response.data.curpage = curpage
    return response.SerializeToString()

@nodeHandle
def NpcShopBuyItem_219(dynamicId,request_proto):
    '''购买，回购商品'''
    argument = NpcShopBuyItem_pb2.npcShopBuyItemRequest()
    argument.ParseFromString(request_proto)
    response = NpcShopBuyItem_pb2.npcShopBuyItemResponse()
    
    
    characterId = argument.id
    itemId = argument.itemId
    opeType = argument.opeType
    buyNum = argument.buyNum
    npcId = argument.npcId
    result = shop.NpcShopBuyItem(dynamicId, characterId, itemId, opeType,buyNum,npcId)
    response.result = result.get('result',False)
    response.message = result.get('message','')
    response.data.opeType = opeType
    return response.SerializeToString()
    
